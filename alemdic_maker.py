import pymongo, json, pickle, re
from bson.json_util import dumps

mdp = ""
with open('../private/dbmdp','r') as infile:
    mdp = infile.read()

mongodb_client = pymongo.MongoClient("mongodb+srv://alemadmin:"+mdp+"@cluster0.6nopd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
print(mongodb_client.list_database_names())

alemdic = mongodb_client['alemdic']

# New Structure
w1 = {
    "w": "mot4055",
    "t": [
        {
            "tt":"trans1",
            "tr":["region1","region2"],
            "tc":"commentaire unique libre"
        },
        {
            "tt":"trans2",
            "tr":["region1"]
        },
        {
            "tt":"trans3",
            "tr":["region1"],
            "ta":[["t","t"],["r","ʁ"],["an","ɑ̃"],["s","s"]]
        }
    ],
    "c": ["lem_cat_souscat_genre_nb_mod_tmps_pers"],
    "f": "fantizi",
    "j": "jiantizi",
    "e": "english meaning"
}

# TO JSON
# print(dicoEn.find())
# strdic = dumps(dicoEn.find())
# print(strdic)
# jsondic = json.loads(strdic)
# print(jsondic[1]["mot4055"]["t"]["trans1"]["r"])


#### DICO EN ####
'''
dicoEn = alemdic['dicoEn']

# SUPPRESSION DU CONTENU SUR CLOUD
print("Suppression du contenu de dicoEn sur le Cloud...")
cpt = 0
for w in dicoEn.find():
    print("Removing... {} - {}".format(cpt, w['w'])+" "*70, end='\r', flush=True)
    dicoEn.delete_one(w)
    cpt+=1
print(cpt, "entrées supprimées.")


# IMPORT DU VIEUX DICTIONNAIRE (json) POUR INSERTION DANS dicoEn
oldDic = {}

print("Chargement du vieux dico...")
with open('dico_enWikiCmuBritfone.json','r') as infile:
    oldDic = json.load(infile)
print("Chargement du vieux dico terminé.")

cpt=0
for w in oldDic.keys():
    cpt+=1
print(cpt, "words in the oldDic.")

cur = 0
for w, tt in oldDic.items():
    if cur<200000:
        cur+=1
        xmot = {
            "w": w,
            "t": []
        }
        for t, r in tt.items():
            xmot["t"].append( { 'tt':t, "tr": r } )

        dicoEn.insert_one(xmot)

print(cur, "mots importés.")
'''


#### DICO FR ####

dicoFr = alemdic['dicoFr']

# SUPPRESSION DU CONTENU SUR CLOUD
print("Suppression du contenu de dicoFr sur le Cloud...")
cpt = 0
for w in dicoFr.find():
    print("Removing... {} - {}".format(cpt, w['w'])+" "*30, end="\r", flush=True)
    dicoFr.delete_one(w)
    cpt+=1
print(cpt, "entrées supprimées.")


# IMPORT DU VIEUX DICTIONNAIRE (json) POUR INSERTION DANS dicoFr
oldDic = {}

print("Chargement du vieux dico...")
with open('dico_frwiktionary-20200301_v2.json','r') as infile:
    oldDic = json.load(infile)
print("Chargement du vieux dico terminé.")

cpt=0
for w in oldDic.keys():
    cpt+=1
print(cpt, "words in the oldDic.")

print("Chargement de MorphDic.pickle...")
# morphDic = pickle.load(open('../../RESSOURCES_TAL/Morphalou3.1_formatCSV_toutEnUn/morphDic.pickle', 'rb'))
morphDic = pickle.load(open('morphDic.pickle', 'rb'))
print("OK")

cur = 0
for w, tt in oldDic.items():
    if cur<2000000 and len(w.split(' '))==1:
        cur+=1
        xmot = {
            "w": w,
            "t": []
        }
        for t in tt:
            xmot["t"].append({
                        "tt": t,
                        "tr": ['FR']
                    })
        if w in morphDic.keys():
            xmot['c'] = morphDic[w]['c']
            
            templi = []
            for li in xmot["t"]:
                templi.append(li['tt'])
            
            for api in morphDic[w]['t']:
                if api not in templi and len(api)>0 and api[-1] != 'ə':
                    xmot['t'].append({
                        "tt": api,
                        "tr": ['FR']
                    })
        dicoFr.insert_one(xmot)
        print("Processing... {}/1414843 - {}".format(cur, w)+" "*30, end="\r", flush=True)

print('\n',cur, "mots importés depuis oldDic.")

cur = 0
for w, i in morphDic.items():
    if cur<2000000 and w not in oldDic.keys():
        ok = False # ajouter le mot que si il a une transcription phonétique
        for t in i['t']:
            if len(t)>0:
                ok = True
        if ok:
            cur+=1
            xmot = {
                "w": w,
                "t": []
            }
            for t in i['t']:
                if len(t)>0:
                    if t[-1] == 'ə':
                        t = t[:-1]
                    xmot["t"].append({
                        "tt": t,
                        "tr": ['FR']
                    })
            
            xmot['c'] = morphDic[w]['c']
            dicoFr.insert_one(xmot)
            print("Ajout depuis morphDic : {} - {}".format(cur, w)+" "*70, end='\r', flush=True)

print(cur, "mots ajoutés depuis morphDic.")


# FR :
# 1414843 mots importés depuis oldDic.                                                                       
#   42989 mots ajoutés depuis morphDic.

# Collection Size: 186.72MB  Total Documents: 1457832

# Nouvelle version (mot unique) :
# Processing... 1350757/1414843 - graphémique                                                                      
#  1350757 mots importés depuis oldDic.
# 42989 mots ajoutés depuis morphDic.ninervée


#### DICO ZH ####
'''
dicoZh = alemdic['dicoZh']
cpt = 0
q = dicoZh.find()
for w in q:
    cpt+=1
print(cpt, "entrées actuellement sur le Cloud.")

# SUPPRESSION DU CONTENU SUR CLOUD
print("Suppression du contenu de dicoZh sur le Cloud...")
cur = 0
q = dicoZh.find()
for w in q:
    print("Removing... {}/{} - {}".format(cur,cpt,w['w'])+" "*70, end='\r', flush=True)
    dicoZh.delete_one(w)
    cur+=1
print(cur, "entrées supprimées.")


# IMPORT DU VIEUX DICTIONNAIRE (json) POUR INSERTION DANS dicoZh
oldDic = {}

print("Chargement du vieux dico...")
with open('dico_zhCCDict_20201206_v1.json','r') as infile:
    oldDic = json.load(infile)
print("Chargement du vieux dico terminé.")

cpt=0
for w in oldDic.keys():
    cpt+=1
print(cpt, "words in the oldDic.")

cur = 0
for w, tt in oldDic.items():
    if cur<200000:
        cur+=1
        xmot = {
            "w": w,
            "t": [
                {   
                    'tt': tt['p'].lower(),
                    'tr': ["CN"]
                }
            ],
            "f": tt['t'],
            "j": tt['s'],
            "e": tt['m']
        }

        if len(re.findall('(Tw)', tt['m']))>0:
            xmot["t"][0]['tr'] = ["TW"]
        
        if len(re.findall('(HK)', tt['m']))>0:
            xmot["t"][0]['tr'] = ["HK","CN"]
        
        dicoZh.insert_one(xmot)
        print("Processing... {}/188996 - {}".format(cur, w)+" "*70, end='\r', flush=True)

print('\n')
print(cur, "mots importés.")
'''



###### RÉPARATION ALEMDIC (mot[t] = ['t':'', 'r':''] → 'tt':'','tr':'')
'''
print('Correction dico Zh:')
dico = alemdic['dicoZh']
cur = 0
curmodif = 0
lastmod = ""
tot = 0
for i in dico.find():
    tot+=1
    print("Counting...",tot, end="\r", flush=True)
print(tot, 'Démarrage...')

for w in dico.find():
    cur+=1
    modif = False
    for t in w['t']:
        if 'r' in t.keys():
            modif = True
            # print("à modifier :", w['w'])
            curmodif +=1
    
    if modif:
        curmodif+=1
        updt = w['t']

        for ut in updt:
            if 'r' in ut:
                ut['tr'] = ['TW', 'CN']
                ut.pop('r', None)
        
        dico.update_one({"w":w['w']}, {"$set": {'t': updt}})
        lastmod = w['w']
    
    print("Processing... {}/{} - nb modifs:\t {}\t {}".format(cur, tot, curmodif, lastmod)+" "*50, end="\r", flush=True)
'''


##### CORRECTION DICO FR + MODIF TRAITS
'''
dico = alemdic['dicoFr']

id2trait = {}
with open('id2trait.json', 'r') as inf:
    id2trait = json.load(inf)
keys = []
for k in id2trait.keys():
    keys.append(k)

print("Chargement de MorphDic.pickle...")
morphDic = pickle.load(open('../../RESSOURCES_TAL/Morphalou3.1_formatCSV_toutEnUn/morphDic.pickle', 'rb'))
#morphDic = pickle.load(open('morphDic.pickle', 'rb'))
print("OK")


cur = 0
traitmodif = 0
curmodif = 0
lastmod = ""
tot = "-"
# for i in dico.find():
#     tot+=1
#     print("Counting...",tot, end="\r", flush=True)
# print(tot, 'Démarrage...')

for w in dico.find():
    cur+=1
    
    # Correction t,r → tt,tr
    modif = False
    for t in w['t']:
        if 'r' in t.keys() or 't' in t.keys():
            modif = True
    
    if modif:
        curmodif+=1
        updt = w['t']

        for ut in updt:
            if 'r' in ut:
                ut['tr'] = ut['r']
                ut.pop('r', None)
            if 't' in ut:
                ut['tt'] = ut['t']
                ut.pop('t', None)
        
        dico.update_one({"w":w['w']}, {"$set": {'t': updt}})
        lastmod = w['w']

    
    # Modif traits
    if 'c' in w.keys() and w['c'] != morphDic[w['w']]['c']:
        dico.update_one({"w":w['w']}, {"$set": {'c': morphDic[w['w']]['c']}})
        traitmodif +=1
    
    print("Processing... {}/{} - nb modifs:\t {}\t {} | traitModif:\t {}".format(cur, tot, curmodif, lastmod, traitmodif)+" "*30, end="\r", flush=True)
    


'''