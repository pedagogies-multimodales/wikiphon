import re
import json

extr = "enWikiExtract.xml"
ful = "enwiktionary-latest-pages-articles-multistream.xml"
outfile = "dico_enWiktionary-20200704_v1.json"

title2local2trans = {}
local2freq = {}
title = ''
cptTitle = 0 # number of pages detected
cptTitleWithIpa = 0 # number of pages with an IPA pattern (all titles to lower case)
cptTrans = 0 # number of transcriptions of english words in the whole Wiktionary

with open(ful,"r") as f:
    for line in f:
        line = line.strip()
        
        # IF THIS LINE HAS A TITLE PATTERN 
        #   → save the current title for later
        searchTitle = re.match(r'^.*<title[^>]*>(.*?)</title>.*$',line)
        if searchTitle:
            cptTitle += 1
            title = searchTitle.group(1).lower()
        
        # IF THIS LINE HAS AN IPA TRANSCRIPTION PATTERN 
        #   → we'll analyze then more precisely what's in this line
        searchTrans = re.match(r'^.*{{IPA\|en\|([^}]*).*$',line)
        if searchTrans:
            if title not in title2local2trans.keys():
                title2local2trans[title] = {}
                cptTitleWithIpa += 1

            # 1) SEARCH FOR LOCALITY PATTERN
            searchLocal = re.findall(r'{{a\|([^}]*)}}',line)
            if searchLocal:
                if len(searchLocal)>1:
                    print(line)
                for locs in searchLocal:
                    locs = locs.split('|') # there might be several localities

                    # just for records
                    for loc in locs:
                        # Increment this locality for records (local2freq)
                        if loc not in local2freq.keys(): 
                            local2freq[loc] = 0
                        local2freq[loc] += 1

            # 2) SEARCH FOR ALL IPA TRANSCRIPTION PATTERNS
            searchAllTrans = re.findall(r'{{IPA\|en\|([^}]*)',line)
            for transs in searchAllTrans:
                transs = transs.split('|') # there might be several transcriptions
                
                # For each transcription, save it to the dictionary in the corresponding locality if there is one
                for trans in transs:
                    cptTrans+=1
                    for loc in locs:
                        if loc not in title2local2trans[title].keys(): title2local2trans[title][loc] = []
                        title2local2trans[title][loc].append(trans)
                        
            # PRINT LINE RESULT
            print(title, locs, transs)

print("cptTitle:",cptTitle, "ctpTitleWithIpa:",cptTitleWithIpa)
for i,j in sorted(local2freq.items(), key=lambda t:t[1], reverse=True):
    print(i,j)

with open(outfile, "w") as outf:
    json.dump(title2local2trans, outf, ensure_ascii=False, indent=4)
print("OK.")