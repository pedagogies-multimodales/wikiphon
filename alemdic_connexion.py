import pymongo

def connexion():
    mdp = ""
    with open('../private/dbmdp','r') as infile:
        mdp = infile.read()

    print('Connexion à AlemDic...')
    mongodb_client = pymongo.MongoClient("mongodb+srv://alemadmin:"+mdp+"@cluster0.6nopd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    print(mongodb_client.list_database_names())
    print('OK!')

    return mongodb_client['alemdic']
    # alemdic = connexion()
    # puis : dicoEn = alemdic['dicoEn']


def printq(q,indent=0):
    # print dictionnaire (ex. résultat de query_one)
    for i, j in q.items():
        if type(j)==dict:
            print('\t'*indent, i, '=> {')
            printq(j,indent+1)
            print('\t'*indent, '}')
        else:
            print('\t'*indent, i, '=>', j)