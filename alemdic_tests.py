import pymongo, json, spacy
from bson.json_util import dumps

print('Initialisation Spacy...')
nlpEn = spacy.load("en_core_web_sm")
print('OK.')

mdp = ""
with open('../private/dbmdp','r') as infile:
    mdp = infile.read()

print('Connexion Cloud...')
mongodb_client = pymongo.MongoClient("mongodb+srv://alemadmin:"+mdp+"@cluster0.6nopd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
print(mongodb_client.list_database_names())
print('OK.')

alemdic = mongodb_client['alemdic']
dicoEn = alemdic['dicoEn']


# print("Chargement du vieux dico...")
# with open('dico_enWikiCmuBritfone.json','r') as infile:
#     oldDic = json.load(infile)
# print("Chargement du vieux dico terminé.")
# cpt=0
# for w in oldDic.keys():
#     cpt+=1
# print(cpt, "words in the oldDic.")

print("Mise en mémoire de dicoEn...")
query = dicoEn.find()
newDic = {}
for q in query:
    if q['w'] not in newDic.keys():
        newDic[q['w']] = q['t']
    else:
        print('DOUBLON DANS DICOEN :', q['w'])
print("Mise en mémoire terminée.")

cpt=0
for w in newDic.keys():
    cpt+=1
print(cpt, "words in the newDic.")


text = """
The placement test SELF (Système d'Évaluation en Langues à visée Formative) is an online low-stake assessment tool with formative orientation. It was developped at Grenoble Alpes University within the frame of the wider IDEFI project Innovalangues, founded by the French Research Agency from 2012 to 2020. SELF was designed to meet a national expectation in tems of formative assessment, by offering a research-backed multilingual test, assessing 3 macro skills: listening, reading and limited writing. Since 2017, SELF is spread out in 6 languages (English, Italian, Spanish, French, Mandarin and Japanese) and placed more than 150.000 students through about 40 French universities and Grandes Écoles.
Since 2018, Innovalangues works on making better the assessment device by developping new contents, optimising the test administration plateform, or building new interdisciplinary partnerships with statisticians, neuro-scientists and IT specialists. Among those research perspectives, we consider developping and piloting a tool for semi-automatic diagnosis of oral production using natural language processing methods.
Automatic assessment of oral production is quite a challenge. On one hand it requires a robust IT infrastructure able to store and process audio recordings for each test administration (i.e. tens of thousands audio files), on the other hand current technologies still struggles to recognise non-native speech. Concerning pronounciation, promising work has been done on little scale to evaluate the quality of realisation of certain phonems with and without automatic speech recognition (ASR) technologies (Harrison et al. 2009, Coulange 2016), word stress by combining duration, intonation and intensity measures (Chen et Wang 2010 ; Chen et Jang 2012; Deshmukh et Verma 2009; Tepperman et Narayaanan 2005) and more rarely MFCC coeficients (Ferrer et al. 2015; C. Li et al. 2007; Shahin et al. 2016), or speech fluency (Bhat et al. 2010, Fontan et al. 2018, Coulange et Rossato 2020a, 2020b). But oral production assessment also needs to focus on what is said and how it is said, and is limited by ASR systems quality, largely impacted by pronunciation quality, variability of productions and the potential influence of other languages known by the learner.
"""

nlpText = nlpEn(text)

print('Début requêtage...')

## DICO JSON ACTUEL
# for token in nlpText:
#     if token.text.lower() in oldDic.keys():
#         print(list(oldDic[token.text.lower()].keys())[0])
#     else:
#         print(token)

## REQUÊTAGE MOT PAR MOT (très long)
# for token in nlpText:
#     query = dicoEn.find_one({"w":token.text.lower()})
#     if query:
#         print(list(query['t'].values())[0])
#     else:
#         print(token)

## DICOEN EN MEMOIRE VIVE (très rapide)
for token in nlpText:
    if token.text.lower() in newDic.keys():
        print(list(newDic[token.text.lower()].keys())[0])
    else:
        print(token)

print("Fin requêtage.")
