# -*- encoding:utf8 -*- 

######### wiktionaryDumpParser.py #########
#
# Version Python 3.7.1
#
# Parseur de dump du wiktionnaire (fr) pour générer un
# dictionnaire phonétisé du français
#
# Sylvain Coulange (2020)
# cf. README pour explications détaillées

import re
import sys
import json

fileIn = '../dump_Wiktionary/frwiktionary-20200301-pages-articles-multistream.xml'
fileOut = 'dico_frwiktionary-20200301_v2.json'

titre2api = {}
i = 0

with open(fileIn, mode="r") as dumpFi:

	for line in dumpFi:
		
		search = re.search(r"'''([^'{}]*)'''\s*({{h( aspiré)*(\|\*)*}})*\s*{{pron\|([^\|]*)\|fr}}",line)
		
		if search:
			titre = search.group(1)
			titre = titre.lower()
			
			listTrans = re.findall(r"({{h( aspiré)*(\|\*)*}})*\s*{{pron\|([^\|]*)\|fr}}",line) # liste des prononciations sur la ligne (grp4=api, grp1=aspi)
			i+=1
			print(i,titre,listTrans)
			
			for trans in listTrans:
				aspi = trans[0]
				api = trans[3]
				
				if len(titre)>0 and len(api)>0:
					if len(aspi)>0:
						api = '*'+api

					if titre not in titre2api.keys():
						titre2api[titre] = []
						titre2api[titre].append(api)
					else:
						if api not in titre2api[titre]:
							titre2api[titre].append(api)
		
with open(fileOut, 'w') as outfile:
    json.dump(titre2api, outfile, ensure_ascii=False)

